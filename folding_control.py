import socket
import json


def read_response(sock, end_seq=b"> ", ignore_start=b"\n", ignore_end_seq=True):
    text = b""
    while True:
        char = sock.recv(1)
        text += char
        if text[-len(end_seq):] == end_seq:
            break
    if ignore_start and text.startswith(ignore_start):
        text = text[len(ignore_start):]
    if ignore_end_seq:
        text = text[:-len(end_seq)]
    return text


def send_and_receive(messages:list, host:str, port:int, timeout=2, password=None) -> list:
    if type(messages) == str:
        messages = [messages]
    responses = {}
    with socket.socket() as s:
        s.settimeout(timeout)
        s.connect((host, port))
        # s.setblocking(0)
        responses['[ON CONNECTION]'] = read_response(s)
        if password is not None:
            messages.insert(0, 'auth {}'.format(password))
        for message in messages:
            s.send(message.encode('ascii') + b'\n')
            responses[message] = read_response(s)
    return responses

