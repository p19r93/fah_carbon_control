import requests


def get_carbon_intensity(postcode=None):
    url = "https://api.carbonintensity.org.uk/"
    if postcode is None:
        extension = "intensity"
    else:
        extension = "regional/postcode/{}".format(postcode)
    response = requests.get(url+extension)
    
    if postcode is None:
        return response.json()['data'][0]['intensity']['forecast']
    else:
        return response.json()['data'][0]['data'][0]['intensity']['forecast']
