# fah_carbon_control

Pause or resume network FoldingAtHome clients according to how dirty the UK grid is currently (its current carbon intensity).

Uses the superb open API carbonintensity.org.uk and the excellent Folding@Home scientific computation contribution program.