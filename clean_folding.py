#!/usr/bin/python3

import os
import argparse
from folding_control import send_and_receive as folding_send_and_receive
from carbon_intensity import get_carbon_intensity


def is_valid_file(parser, arg):
    if not os.path.exists(arg):
        parser.error("The file %s does not exist!" % arg)
    else:
        return arg  # return an open file handle

    
parser = argparse.ArgumentParser(description="Pause or resume local network FAH clients based on grid carbon intensity")

parser.add_argument("--postcode",
                    "-p",
                    type=str,
                    help="Postcode stub for regional carbon data")

parser.add_argument("--hostsfile",
                    type=lambda x: is_valid_file(parser, x),
                    default="hosts.txt",
                    help="File containing hosts and ports of FAH clients")

parser.add_argument("--cutoff",
                    "-c",
                    type=int,
                    default=80,
                    help="Maximum carbon intensity to continue folding")

args = parser.parse_args()

with open(args.hostsfile, 'r') as f:
    folding_hosts = []
    for line in f:
        vals = line.split()
        if len(vals) == 3:
            folding_hosts.append((vals[0], int(vals[1]), vals[2]))
        else:
            folding_hosts.append((vals[0], int(vals[1])))

print("HOSTS:", folding_hosts, sep="\n")

#cutoff = 80  # g/kWh below which to fold
cutoff = args.cutoff

#postcode = 'DH1'

if args.postcode:
    postcode = args.postcode
    current_carbon_intensity = get_carbon_intensity(postcode=postcode)
else:
    current_carbon_intensity = get_carbon_intensity()

regional_national_description = "regional ({})".format(postcode) if args.postcode else "national"
print("Current {} grid carbon intensity: {} gCO2e/kWh".format(regional_national_description, current_carbon_intensity))

if current_carbon_intensity < cutoff:
    print("Folding will continue (current intensity {} below cutoff {})".format(current_carbon_intensity, cutoff))
    command = 'unpause'
else:
    print("Folding will be paused (current intensity {} at or above cutoff {})".format(current_carbon_intensity, cutoff))
    command = 'pause'

for host, port, *rest in folding_hosts:
    if rest:
        password = rest[0]
    print("Contacting host {}...".format(host))
    try:
        responses = folding_send_and_receive(command, host, port, password=password)
        print(host, responses[command], sep=' : ')
    except (ConnectionError, OSError, TimeoutError):
        print(host, "Could not connect", sep=' : ')
    except:
        print("Unhandled exception encountered.")
    
